from datetime import date

import flask
import werkzeug
from flask import request , jsonify
from flask_jwt_extended import (jwt_required , get_jwt_identity)
from app.api import bp
from app.api.errors import bad_request
from app.projects.antigen_ocr.antigen import lftOCR
from app.projects.antigen_ocr.antigen_model import LFT_OCR_Model
from flask import current_app as app

from app.projects.antigen_ocr.predictor import predict

CANNOT_DETECT_DIGITS_ERROR = 100
@bp.route ( '/lftimg' , methods=['POST'] )
@jwt_required()
def pocpic():
    if request.headers['Content-Type'] == 'application/json':
        data = request.get_json() or {}
    else:
        data = request.form.to_dict() or {}
    app.logger.info (
        "Data receive: {}".format(data))
    if not flask.request.files['antigen_image']:
        app.logger.info("must include content type and photo data in request")
        return bad_request('must include content type and photo data in request')
    antigen_image = flask.request.files['antigen_image']

    app.logger.info("Image: {}".format(antigen_image))
    filename = werkzeug.utils.secure_filename(antigen_image.filename)
    antigen_image.save(filename)
    
    results = predict(filename)

    ret = {
        "result": results[0],
    }
    return jsonify(ret), 200


@bp.route ( '/lftocresult' , methods=['POST'] )
@jwt_required()
def pocResult():
    """Creates record of antigen results from JSON data in the request."""
    if request.headers['Content-Type'] == 'application/json' :
        data = request.get_json () or {}
    else :
        data = request.form.to_dict () or {}

    app.logger.info("Data receive: {}".format(data))
    current_user = get_jwt_identity()
    model = lftOCR._save_data(data, current_user)
    app.logger.info("Model saved: {}".format(model))
    return jsonify(model), 201


@bp.route ( '/getlftresult' )
@jwt_required()
def getPocRecords () :
    """Retrieves records of a user."""
    current_user = get_jwt_identity ()
    return jsonify ( LFT_OCR_Model.objects ().filter ( user_id=current_user['id'] ) )


@bp.route ( '/lftimg/<doc_id>' , methods=['DELETE'] )
@jwt_required()
def poc_delete_from_id ( doc_id ) :
    """Deletes records corresponding to the given ID."""
    current_user = get_jwt_identity ()
    LFT_OCR_Model.objects.get_or_404 ( id=doc_id , user_id=current_user['id'] ).delete ()

    return jsonify ( {"success" : True})
