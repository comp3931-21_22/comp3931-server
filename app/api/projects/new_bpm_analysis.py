from datetime import date

import flask
import werkzeug
from flask import request , jsonify
from flask_jwt_extended import (jwt_required , get_jwt_identity)
from app.api import bp
from app.api.errors import bad_request
from app.projects.new_bpm_ocr.bpm import bpmOCR
from app.projects.new_bpm_ocr.bpm_model import BPM_OCR_Model
from flask import current_app as app

from app.projects.new_bpm_ocr.predictor import predict

CANNOT_DETECT_DIGITS_ERROR = 100
@bp.route ( '/bpmimg' , methods=['POST'] )
@jwt_required()
def bpmpic():
    if request.headers['Content-Type'] == 'application/json':
        data = request.get_json() or {}
    else:
        data = request.form.to_dict() or {}
    app.logger.info (
        "Data receive: {}".format(data))
    if not flask.request.files['monitor_image']:
        app.logger.info("must include content type and photo data in request")
        return bad_request('must include content type and photo data in request')
    monitor_image = flask.request.files['monitor_image']

    app.logger.info("Image: {}".format(monitor_image))
    filename = werkzeug.utils.secure_filename(monitor_image.filename)
    monitor_image.save(filename)
    results = predict(filename)

    ret = {
        "systolic": results[0],
        "diastolic": results[1],
        "pulse": results[2],
    }
    return jsonify(ret), 200


@bp.route ( '/bpmocresult' , methods=['POST'] )
@jwt_required()
def bpmResult():
    """Creates record of Blood pressure results from JSON data in the request."""
    if request.headers['Content-Type'] == 'application/json' :
        data = request.get_json () or {}
    else :
        data = request.form.to_dict () or {}

    app.logger.info("Data receive: {}".format(data))
    current_user = get_jwt_identity()
    model = bpmOCR._save_data(data, current_user)
    app.logger.info("Model saved: {}".format(model))
    return jsonify(model), 201


@bp.route ( '/getbpmresult' )
@jwt_required()
def getBPMRecords () :
    """Retrieves records of a user."""
    current_user = get_jwt_identity ()
    return jsonify ( BPM_OCR_Model.objects ().filter ( user_id=current_user['id'] ) )


@bp.route ( '/bpmocresult/<doc_id>' , methods=['DELETE'] )
@jwt_required()
def poc_delete_from_id ( doc_id ) :
    """Deletes records corresponding to the given ID."""
    current_user = get_jwt_identity ()
    BPM_OCR_Model.objects.get_or_404 ( id=doc_id , user_id=current_user['id'] ).delete ()

    return jsonify ( {"success" : True})
