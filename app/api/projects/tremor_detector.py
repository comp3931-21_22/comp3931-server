from http.client import BAD_REQUEST
from flask import request, g, make_response, jsonify
from flask_jwt_extended import (jwt_required, get_jwt_identity)
import pandas as pd
import numpy as np

from app.api import bp
from app.api.errors import bad_request
from app.projects.tremor_detector.raw_data_parsing import parseAccelerometer, parseMagnetometer, parseGPS
from app.projects.tremor_detector.preprocessing import preprocessing
from app.projects.tremor_detector.tremor_recordings_model import saveFeatureVector, getFeatureVectors, getTremorResult


@bp.route('/tremor-detector/uploadcsv/', methods=['POST'])
@jwt_required()
def uploadCSV():
    current_user = get_jwt_identity()
    if 'application/json' in request.headers['Content-Type']:
        data = request.get_json() or {}
    else:
        data = request.form.to_dict() or {}
    if current_user != None:
        if data['accelerometer'] == "" or data['magnetometer'] == "":
             return jsonify(), 200
        accel = parseAccelerometer(data['accelerometer'])
        compass = parseMagnetometer(data['magnetometer'])
        gps = parseGPS(data['gps'])
        featureVector = preprocessing(accel, compass, gps)
        if featureVector != None:
            saveFeatureVector(featureVector, current_user['id'])
            return jsonify(), 201
        return jsonify(), 200
    else:
        response = make_response("Please Login", 401)
        response.mimetype = "text/plain"
        return response

@bp.route('/tremor-detector/getinstances', methods=['GET'])
@jwt_required()
def getInstances():
    current_user = get_jwt_identity()
    if current_user != None:
        return jsonify({"instances": len(getFeatureVectors(current_user['id']))}), 200
    else:
        return bad_request("Please log in")

@bp.route('/tremor-detector/result', methods=['GET'])
@jwt_required()
def getResult():
    current_user = get_jwt_identity()
    if current_user != None:
        result = getTremorResult(current_user['id'])
        return jsonify({"result": result}), 200
    else:
        return bad_request("Please log in")