from app.models.project_base import ProjectBase
from app import db_mongo as db


class LFT_OCR_Model(ProjectBase):
    """Document definition for the Point of Care OCR project.
        Stores the results from a point of care device (Blood pressure monitor).
        Including time of the test, systolic value, diastolic value and heart rate of user
    """

    possible_labels = {}
    result = db.StringField(required=True)


