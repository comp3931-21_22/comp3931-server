import operator
import os

import cv2
import imutils
import numpy as np
from tensorflow.keras.models import load_model
def predict(filename):
    user_img = cv2.imread(filename)
    user_input = [] 
    img_preprocessed = pre_process_single(filename)
    user_input.append(img_preprocessed)
    df_input = np.asarray(user_input)
    df_input = df_input/255.0

    model = load_model("app/projects/new_bpm_ocr/model.h5")
    user_prediction = model.predict(df_input)
    rounded_predictions = np.round(user_prediction)
    if(int(rounded_predictions)==1):
        prompt = "Positive"
    else:
        prompt = "Negative"

    outcome = []
    outcome.append(prompt)
    os.remove(filename)
    return outcome



def pre_process_single(input_img):
    input_img = Image.fromarray(input_img,"RGB")
    input_img = input_img.resize((300,300)) #resize to 300x300
    #final = cv2.normalize(input_img, norm, 0, 255, cv2.NORM_MINMAX) #normalised
    return np.array(input_img)