import numpy as np
import pandas as pd
import warnings
from scipy.signal import welch
from io import StringIO

def calculateRMS(array):
    return np.sqrt(np.mean(np.square(array)))

def parseAccelerometer(csvString):

    table = []

    accel = pd.read_csv(StringIO(csvString), header=None).to_numpy()

    prev_time = -1

    current_data = []

    for a in accel:
        
        a[0] = str(a[0])

        try:
            time = int(a[0].split(' ')[1].split(':')[2])
        except:
            continue

        if time == prev_time:

            # format time string and append data to current data array
            formatted_time = a[0].replace('-', '/')
            current_data.append([formatted_time, float(a[1]), float(a[2]), float(a[3])])

        else:
            prev_time = time
            if len(current_data) > 35:
                
                current_data = np.asarray(current_data)
                row = []
                std = []
                std.append(np.std(current_data[:, 1].astype(float))) # x.standard.deviation
                std.append(np.std(current_data[:, 2].astype(float))) # y.standard.deviation
                std.append(np.std(current_data[:, 3].astype(float))) # z.standard.deviation

                row.append(current_data[-1][0].split(' ')[1])
                row.append(calculateRMS(std))

                #calculate psd
                powerSpec = []
                for i in range(1,4):
                    with warnings.catch_warnings():
                        warnings.simplefilter("ignore")
                        freq, psd = welch(current_data[:, i].astype(float), fs=len(current_data[:, i]))
                    
                    prev_freq = 0

                    psd6 = 0
                    for f in range(len(freq)):
                        if freq[f] < 6:
                            prev_freq = f
                        elif freq[f] == 6:
                            psd6 = psd[f]
                            break
                        else:
                            diff = psd[f] - psd[prev_freq]
                            psd6 = psd[prev_freq] + (diff *(6-freq[prev_freq])/((6-freq[prev_freq])+(freq[f]-6)))
                            break
                    powerSpec.append(psd6)
                
                row.append(calculateRMS(powerSpec)) 

                current_data = []

                row = np.asarray(row, dtype="O")

                table.append(row)
    
    return np.asarray(table, dtype="O")

def parseMagnetometer(csvString):

    table = []

    compass = pd.read_csv(StringIO(csvString), header=None).to_numpy()

    prev_time = -1

    current_data = []

    for c in compass:

        c[0] = str(c[0])

        try:
            time = int(c[0].split(' ')[1].split(':')[2])
        except:
            continue

        if time == prev_time:

            # format time string and append data to current data array
            formatted_time = c[0].replace('-', '/')
            current_data.append([formatted_time, float(c[1]), float(c[2]), float(c[3])])

        else:
            prev_time = time
            if len(current_data) > 0:
                
                current_data = np.asarray(current_data)
                row = []
                std = []
                std.append(np.std(current_data[:, 1].astype(float))) # x.standard.deviation
                std.append(np.std(current_data[:, 2].astype(float))) # y.standard.deviation
                std.append(np.std(current_data[:, 3].astype(float))) # z.standard.deviation

                row.append(current_data[-1][0].split(' ')[1])
                row.append(calculateRMS(std))


                current_data = []

                row = np.asarray(row, dtype="O")

                table.append(row)

    return np.asarray(table, dtype="O")

def parseGPS(csvString):

    if csvString == "":
        return []

    gps = pd.read_csv(StringIO(csvString), header=None).astype({
        1: float,
        2: float,
        3: float
    }).to_numpy()

    for g in range(len(gps)):

        gps[g, 0] = str(gps[g, 0])
        gps[g, 0] = gps[g, 0].replace('-', '/').split(' ')[1]

    return gps