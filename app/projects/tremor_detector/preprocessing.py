import numpy as np
from sklearn.cluster import KMeans

from app.projects.tremor_detector.quantization import quantization

time_id = 0
time_dict = {}

def calculateRMS(array):
    return np.sqrt(np.mean(np.square(array)))


def timeToInt(timeString):
    return float(timeString.replace(':', ''))

# assign int ID to time value
def assignTimeID(timeString):
    global time_id, time_dict
    if timeString in time_dict:
        return time_dict[timeString]
    time_dict[timeString] = time_id
    time_id += 1
    return time_id-1

# binary search to find instance of specific time in array
def findTime(time, array):
    index = int(len(array)/2)
    array_time = timeToInt(array[index][0])
    if array_time == time:
        return array[index]
    elif index == 0:
        return [None]
    elif array_time > time:
        return findTime(time, array[0:index])
    elif array_time < time:
        return findTime(time, array[index:])

def preprocessing(accel, compass, gps):

    global time_id, time_dict

    time_id = 0
    time_dict = {}

    file_length = 600
    min_cluster_length = 120
    num_clusters = 10

    # array of instances to cluster
    all_instances = []
    instances = []


    if len(accel) < file_length or len(compass) < file_length:
        return
    
    for a in range(len(accel)):
        time = accel[a, 0]
    
        found = findTime(timeToInt(time), compass)
        if len(found) <= 1:
            continue
        all_instances.append([accel[a][1], found[1]])

        if len(gps) >= file_length:
            found = findTime(timeToInt(time), gps)
            if len(found) <= 1:
                continue
            instances.append(all_instances[-1])
            instances[-1].extend([found[1], found[2], found[3]])
        else:
            instances = all_instances

        instances[-1].extend([assignTimeID(time)])

    if len(instances) < file_length:
        return
    
    # array of times of instances in each cluster
    times = []
    classes = []
    for i in range(num_clusters):
        times.append([])
        classes.append([])

    # cluster data
    cluster = KMeans(n_clusters=len(classes), random_state=0).fit_predict(instances[:])

    # classes not to be included
    outlier_classes = []

    # sort times of instances in clusters
    for i in range(len(cluster)):
        classes[cluster[i]].append(instances[i][0:-1])
        times[cluster[i]].append(instances[i][-1])      
    
    for c in range(len(classes)):
        # classes are outliers if:

        # if classes have fewer than a number of  instances
        if len(classes[c]) < min_cluster_length:
            outlier_classes.append(c)
            continue

        # if standard deviation of accelerometer is too small or standard deviation compass is too large
        if np.median(classes[c][0]) < 0.02 or np.median(classes[c][1]) > 2.5:
            outlier_classes.append(c)
            continue

        classes[c] = np.asarray(classes[c])

        try:
            # if gps location is too far away from median
            med_x = np.median(classes[c][:, 2])
            med_y = np.median(classes[c][:, 3])
            med_z = np.median(classes[c][:, 4])
            for g in classes[c]:
                if calculateRMS([g[2] - med_x, g[3] - med_y, g[4] - med_z]) > 20:
                    outlier_classes.append(c)
                    break
        except:
            pass

    to_export = []

    for a in accel:
        for t in range(len(times)):
            if assignTimeID(a[0]) in times[t] and t not in outlier_classes:
                to_export.append(np.array([a[1], a[2]]))
                break
    
    to_export = np.asarray(to_export)

    if len(to_export) >= file_length:
        return quantization(to_export)
    else:
        return None