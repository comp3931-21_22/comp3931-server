from joblib import load
from sklearn.ensemble import RandomForestClassifier

from app import db_mongo as db
from app.models.user import User
from app.models.project_base import ProjectBase

class TremorRecordings(ProjectBase):
    user_id = db.IntField()
    data = db.ListField(db.IntField())
    prediction = db.IntField()

def saveFeatureVector(featureVector, current_user_id):
    recording = TremorRecordings()
    recording.user_id = current_user_id
    recording.data = featureVector

    # perform classification
    model = load('app/projects/tremor_detector/model.joblib')
    recording.prediction = model.predict([featureVector])
    recording.save()

def getFeatureVectors(current_user_id):
    featureVectors = TremorRecordings.objects(user_id=current_user_id)
    return featureVectors

def getTremorResult(current_user_id):
    featureVectors = TremorRecordings.objects(user_id=current_user_id)
    num = len(featureVectors)
    if num == 0:
        return -1
    total = 0
    for f in featureVectors:
        total += f.prediction
    return total/num
