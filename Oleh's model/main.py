import numpy as np
import pandas as pd
import os
from glob import glob
import seaborn as sns
from PIL import Image
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg
from matplotlib import pyplot as plt
from keras.models import load_model
from numpy.random import randn

from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Reshape
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Dropout
from keras.layers import AveragePooling2D
from keras.layers import MaxPooling2D


import numpy as np

np.random.seed(42)
from keras.utils.np_utils import to_categorical  # used for converting labels to one-hot-encoding
from sklearn.model_selection import train_test_split
from scipy import stats

def run_generator(n):
    model = load_model('GAN_50_epochs.h5', compile=False)

    vector = randn(100 * n)
    vector = vector.reshape(n, 100)
    X = model.predict(vector)
    X = (X + 1) / 2.0
    X = (X * 255).astype(np.uint8)
    for i in range(len(X)):
        plt.imshow(X[i])
        plt.axis('off')
        plt.savefig('image_dump/fake_' + str(i) + '.jpg')



def run_classifier():
    datagen = ImageDataGenerator()

    dataflow = datagen.flow_from_directory(directory='distributed/',
                                           color_mode='rgb',
                                           class_mode='categorical',
                                           batch_size=10015,
                                           target_size=(64, 64))
    x1, y1 = next(dataflow)

    weights = {0: 1.52,
               1: 1.,
               2: 0.5,
               3: 4.35,
               4: 0.5,
               5: 0.074,
               6: 3.52}


    x1 = x1.astype('float32')
    x1 = np.asarray(x1.tolist())
    x1 = x1 / 255.

    model = Sequential()

    model.add(Conv2D(128, (3, 3), padding='valid', input_shape=(64,64,3)))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, (3, 3), padding='valid', input_shape=(64,64,3)))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(7, activation='softmax'))

    opt = Adam(lr=0.0005, beta_1=0.9)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])

    x_tr, x_test, y_tr, y_test = train_test_split(x1, y1, train_size=0.2, test_size=0.2, random_state=15)
    model1 = model.fit(x_tr,y_tr,epochs=100, validation_data=(x_test, y_test), class_weight=weights)
    score = model.evaluate(x_test, y_test)

    model.save('classifier_model.h5')

    print('Test accuracy:', score[1])
    print(model.summary())

def run_both():
    model2 = load_model('classifier_model.h5', compile=False)

    datagen1 = ImageDataGenerator()

    dataflow1 = datagen1.flow_from_directory(directory='image_dump/',
                                           color_mode='rgb',
                                           class_mode='categorical',
                                           batch_size=1000,
                                           target_size=(64, 64))
    x2, _ = next(dataflow1)

    x2 = x2.astype('float32')
    x2 = np.asarray(x2.tolist())
    x2 = x2 / 255.

    result_vector = model2.predict(x2)




if __name__ == '__main__':
    run_classifier()
    run_generator(10)
    #run_both()

