#!/bin/bash

echo "Generating SSL Key..."

echo "Generating RSA Key and Certificate Request..."
openssl req -x509 -newkey rsa:4096 -days 3650 -keyout rsa-priv.pem -out certificate.crt

openssl rsa -in rsa-priv.pem -out rsa-priv.key
rm rsa-priv.pem

echo "Done. Remember to copy the 'certificate.crt' file to the 'app/src/main/res/raw/' directory in the Android application"