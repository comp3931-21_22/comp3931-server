#!/bin/bash
source venv/bin/activate

flask db upgrade

exec gunicorn -b :5000 --certfile=certificate.crt --keyfile=rsa-priv.key --access-logfile - --error-logfile - base_application:app